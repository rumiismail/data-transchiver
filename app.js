const scheduler = require('./scheduler.js');
const tasks = require('./tasks.js');

var jobs =  [];

var callback = (taskName) => {
  jobs.forEach(job => {
    
    if ( job.id === taskName){
      scheduler.stopSchedule(job.task);

      if (job.id === "DataIdentifier"){
        var _schedule = scheduler.getSchedule(true);
        var job = scheduler.createSchedule(_schedule, function(){
          tasks.DataTransfer(_schedule, callback);
        });  
        jobs.push({"id": "DataTransfer", "task": job});
      }
    }
  });
}

var startSchedule = () => {
  tasks.prepare("DataIdentifier");

  var _schedule = scheduler.getSchedule();
  var job = scheduler.createSchedule(_schedule, function(){
    tasks.DataIdentifier(_schedule, callback);
  });
  
  jobs.push({"id": "DataIdentifier", "task": job});
}

startSchedule();




